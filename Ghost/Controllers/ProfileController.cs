﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ghost.Domain;
using Ghost.Data.Repositories;
using Ghost.Domain.ViewModel;
using Ghost.Service.Interfaces;
using System.Web.Security;
using System.Drawing;
using System.IO;
using System.Web.UI;

namespace Ghost.Controllers
{
    public class ProfileController : Controller
    {

        private readonly ISecutiryService _securityService;
        private readonly IUserService _userSerivce;
        private readonly IImageService _imageService;
        private readonly IGenderRepository _genderRepository;

        public ProfileController(ISecutiryService securityService,
            IGenderRepository genderRepository,
            IUserService userService, 
            IImageService imageService)
        {
            this._securityService = securityService;
            this._genderRepository = genderRepository;
            this._userSerivce = userService;
            this._imageService = imageService;
        }

        [HttpGet]
        [Authorize]
        public ActionResult Index()
        {
            var user = _securityService.GetUserById(Int32.Parse(User.Identity.Name));
            var viewModel = new ProfileViewModel()
            {
                FirstName = user.FirstName,
                LastName = user.LastName,
                Email = user.Email,
                AvatarUrl = user.AvatarURL,
                BirthDay = user.BirthDay,
                PhoneNumber = user.PhoneNumber,
                AboutMe = user.AboutMe,
                DateCreated = user.DateCreated,
                DateLastLogin = user.DateLastLogin,
                FileCounter = user.Files.Count,
            };
            if (user.Storage != null)
            {
                viewModel.StorageCounter = 1;
                viewModel.StorageData = new SmallStorageData()
                {
                    TotalSize = user.Storage.MaxSize,
                    CurentSize = user.Storage.CurrentSize,
                    FreeSize = user.Storage.MaxSize - user.Storage.CurrentSize
                };
            }
            else
            {
                viewModel.StorageCounter = 0;
                viewModel.StorageData = null;
            }

            ViewBag.GenderID = new List<Gender>(_genderRepository.GetAll().Where(x=>x.GenderID<3));
            ViewData["PersonID"] = user.UserId.ToString();
            return View(viewModel);
        }

        [HttpGet]
        [Authorize]
        public ActionResult EditSecuritySetting(ProfileViewModel model)
        {
            int changeIndex = 0;

            var user = _securityService.GetUserById(Int32.Parse(User.Identity.Name));
            try
            {
                if((model.NewPassword!=null)&&(model.ConfirmNewPassword!=null))
                if((model.NewPassword.Replace(" ","") == "")||(model.ConfirmNewPassword.Replace(" ","") == ""))
                    throw new System.ArgumentNullException();
            }
            catch (ArgumentException argEx)
            {
                TempData["errorMessage"] = "При изменении пароля, не указан старый пароль. Пароль не был изменен.";
                return RedirectToAction("Index");
            }

            if ((model.OldPassword != null) && (model.NewPassword.Equals(model.ConfirmNewPassword)))
            {
                if (_securityService.ValidateUser(user.Email, CreatePasswordHash(model.OldPassword, user.Salt)))
                {
                    user.Password = CreatePasswordHash(model.NewPassword, user.Salt);
                    changeIndex += 4;
                }
                else
                {
                    TempData["errorMessage"] = "При изменении пароля, указан не верный старый пароль. Пароль не был изменен.";
                }
            }

            if (model.PhoneNumber != null)
            {
                user.PhoneNumber = model.PhoneNumber;
                changeIndex += 1;
            }
            if (model.Email != null)
            { 
                user.Email = model.Email;
                changeIndex += 2;
            }

            switch (changeIndex)
            {
                case 0:
                    break;
                case 1:
                    _securityService.UpdateUser(user);
                    break;
                case 2:
                case 3:
                case 4:
                case 5:
                case 6:
                case 7:
                    _securityService.UpdateUser(user);
                    return RedirectToAction("SignOut","Account");
                 default:
                        break;
                }

            return RedirectToAction("Index");
        }

        [HttpPost]
        [Authorize]
        public ActionResult EditProfileSetting(ProfileViewModel model,HttpPostedFileBase fileUpload)
        {
            var user = _securityService.GetUserById(Int32.Parse(User.Identity.Name));
            if (model.FirstName != null)
                user.FirstName = model.FirstName;

            if (model.LastName != null)
                user.LastName = model.LastName;

            DateTime nullDate = new DateTime(1, 1, 1, 0, 0, 0);
            if (model.BirthDay != nullDate)
                user.BirthDay = model.BirthDay;

            if (model.AboutMe != null)
                user.AboutMe = model.AboutMe;

            if (model.GenderID != null)
            {
                _userSerivce.AssignGender(user.UserId, model.GenderID);
            }

            if (fileUpload != null)
            {
                //обработка аватара
                Image src = Image.FromStream(fileUpload.InputStream, true, true);
                Image srcThumb = src;
                src = _imageService.ScaleImage(src, 200);
                srcThumb = _imageService.ScaleImage(srcThumb, 30);
                string avatarPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory + "App_Data/" + user.UserId + "/");
                string avatarName;

                user.AvatarURL = avatarPath;
                if (!(Directory.Exists(avatarPath)))
                    Directory.CreateDirectory(avatarPath);

                avatarName = String.Concat("m_",user.UserId.ToString(), ".png");
                src.Save(Path.Combine(avatarPath, avatarName), System.Drawing.Imaging.ImageFormat.Png);
                
                avatarName = String.Concat("s_",user.UserId.ToString(), ".png");
                srcThumb.Save(Path.Combine(avatarPath, avatarName), System.Drawing.Imaging.ImageFormat.Png);

            }

            _securityService.UpdateUser(user);

            return RedirectToAction("Index");
        }

        private static string CreatePasswordHash(string pwd, string salt)
        {
            return FormsAuthentication.HashPasswordForStoringInConfigFile(String.Concat(pwd, salt), "md5");
        }

        [HttpGet]
        [Authorize]
        public ActionResult GetImage(int id, string size)
        {
            var user = _securityService.GetUserById(id);

            string fileName = String.Concat(size, "_", id.ToString(), ".png");
            string path = Path.Combine(user.AvatarURL,fileName );

            return base.File(path, "image/png");
        }
    }
}