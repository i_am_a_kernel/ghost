﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ghost.Domain;
using Ghost.Data.Repositories;
using Ghost.Domain.ViewModel;
using Ghost.Service.Interfaces;
using System.Text;
using System.Web.Services;
using System.IO;
using System.Web.Security;
using PagedList;
using System.Security.Cryptography;

namespace Ghost.Controllers
{
    public class StorageController : Controller
    {
        //
        // GET: /Storage/

        private readonly IStorageService _storageService;
        private readonly IUserRepository _userRepository;
        private readonly IMailSender _mailSender;
        private readonly ISteganographyService _steganographyService;
        private readonly IFileRepository _fileRepository;
        private readonly IPartRepository _partRepository;

        public StorageController(IStorageService storageService
            , IUserRepository userRepository
            , IMailSender mailSender
            , ISteganographyService steganographyService
            , IFileRepository fileRepository
            , IPartRepository partRepository)
        {
            this._storageService = storageService;
            this._userRepository = userRepository;
            this._mailSender = mailSender;
            this._steganographyService = steganographyService;
            this._fileRepository = fileRepository;
            this._partRepository = partRepository;
        }

        [HttpGet]
        [Authorize]
        public ActionResult Index()
        {
            var storages = _storageService.GetStoragesOfUser(Int32.Parse(User.Identity.Name));
            return View(storages.ToList());
        }

        [HttpGet]
        [Authorize]
        public ActionResult UploadFile(int id)
        {
            UploadFileViewModel model = new UploadFileViewModel()
            {
                StorageId = id
            };
            return View(model);
        }

        [HttpPost]
        [Authorize]
        public ActionResult UploadFile(UploadFileViewModel model, HttpPostedFileBase uploadFile)
        {
            if (!(ModelState.IsValid) && (uploadFile == null))
            {
                TempData["ErrorForUser"] = "Вы не выбрали файл для загрузки. Выберите файл и попробуйте еще раз.";
                return View(model);
            }
            try
            {
                SteganographyModel sModel = new SteganographyModel()
                {
                    fileSize = uploadFile.ContentLength
                    ,
                    Layer = 8
                    ,
                    fileName = uploadFile.FileName
                    ,
                    fileNameSize = uploadFile.FileName.Length
                    ,
                    writeFileName = true
                };
                MemoryStream target = new MemoryStream();
                uploadFile.InputStream.CopyTo(target);
                byte[] data = target.ToArray();
                var storage = _storageService.GetStorageById(model.StorageId);

                if(data.Length > (storage.MaxSize-storage.CurrentSize))
                {
                    TempData["ErrorForUser"] = String.Concat("В хранилище не достаточно места для загрузки файла ",sModel.fileName,".");
                    return View(model);
                }

                _steganographyService.UploadFile(data, sModel, model.StorageId);
                TempData["SuccessForUser"] = "Ваш файл успешно загружен.";
                return View(model);
            }
            catch
            {
                TempData["ErrorForUser"] = "В ходе работы произошла ошибка. Попробуйте загрузить файл еще раз. Если не получиться, напишите в поддержку.";
                return View(model);
            }
        }
        
        [HttpGet]
        [Authorize]
        public ActionResult DownloadFile(int id,bool? accept = false)
        {
            TempData["id"] = id;
            var file = _fileRepository.GetById(id);

            ViewData["MD5"] = file.MD5Hash;
            ViewData["SHA1"] = file.SHA1Hash;

            if (accept.Value)
            {
                byte[] fileArray = _steganographyService.DownloadFile(id);
                var outFile = File(fileArray, System.Net.Mime.MediaTypeNames.Application.Octet, file.FileFullName);

                return outFile;
            }
            return View();
        }


        [HttpGet]
        [Authorize]
        public ActionResult DeleteStorage(int id)
        {
            var storage = _storageService.GetStorageById(id);
            var user = _userRepository.GetById(Int32.Parse(User.Identity.Name));
            //Сформировать ключ, оправить письмо, заполнить модель.

            string key = _steganographyService.GetRandomKey(4);
            _mailSender.SendMail(user.Email, "Ghost: Код подтверждения для удаления хранилища"
                , String.Format("Уважаемый {0} {1}! С вашего аккаунта поступила заявка на удаление хранилища, если вы согласны, /n введите на странице код: {2} , иначе проинорируйте письмо."
                    ,user.FirstName,user.LastName,key));
            var model = new DeleteStorageViewMode()
            {
                CurentStorage = storage,
                ValidationKey = String.Empty,
                CoreValidationKey = FormsAuthentication.HashPasswordForStoringInConfigFile(key,"md5"),
                StorageId = storage.StorageId
            };
          
            return View(model);
        }

        [ChildActionOnly]
        public PartialViewResult _FilesListView(int id, int? page)
        {
            var fileList = new PartialFilesListViewModel()
            {
                Files = _fileRepository.GetMany(x => x.Storage.StorageId == id).ToList()
            };

            return PartialView("_FilesListPartial", fileList);
           

        }

        [HttpPost]
        [Authorize]
        public ActionResult DeleteStorage(DeleteStorageViewMode model)
        {
            //Если ключ в модели совпал с ключем отправленным удаляем хранилище
            if (model.CoreValidationKey == FormsAuthentication.HashPasswordForStoringInConfigFile(model.ValidationKey, "md5"))
            {
                List<Part> parts = _partRepository.GetMany(x=>x.Files.Storage.StorageId==model.StorageId).ToList();
                foreach (var item in parts)
                {
                    System.IO.File.Delete(item.CloudPath);
                }
                _storageService.DeleteStorage(model.StorageId);
            }
            else
            {
                model.CurentStorage = _storageService.GetStorageById(model.StorageId);
                return View(model);
            };

            return RedirectToAction("Index");
        }


        [HttpGet]
        [Authorize]
        public ActionResult FilesList(int? id, int? page)
        {
            int pageSize = 10;
            int pageIndex = (page ?? 1);

            var fileList = new FilesListViewModel();
            if (id.HasValue)
            {
                //Костыль
                int tempID = -1;
                if (id > tempID)
                {
                    tempID = id ?? default(int);
                    fileList.Files = _fileRepository.GetMany(x => x.Storage.StorageId == tempID).ToPagedList(pageIndex, pageSize);
                    fileList.StorageName = _storageService.GetStorageById(tempID).StorageName;
                    fileList.StorageId = tempID;
                }
            }
            else
            {
                fileList.Files = _fileRepository.GetMany(x => x.User.UserId == Int32.Parse(User.Identity.Name)).ToPagedList(pageIndex, pageSize);
                fileList.StorageName = String.Empty;
            }

            if (!(Request.HttpMethod == "GET"))
                page = 1;

            return View("FilesList",fileList);
        }


        [HttpGet]
        [Authorize]
        public ActionResult Create()
        {
            var user = _userRepository.GetById(Int32.Parse(User.Identity.Name));

            if (user.Storage != null)
            {
                TempData["ErrorForUser"] = "На данном этапе мы не можем предоставить более одного хранилища. Хранилище не создано!";
                return RedirectToAction("Index");
            }
            else if((user.FirstName==null)||(user.LastName==null)||(user.PhoneNumber==null))
            {
                TempData["ErrorForUser"] = "Для подключения хранилища необходимо что бы профиль был заполнен. Хранилище не создано!";
                return RedirectToAction("Index");
            }

            Random rnd = new Random((int)DateTime.Now.Ticks);
                StringBuilder pass = new StringBuilder();
                for(int i = 0; i<5;i++)
                    pass.Append((Char)rnd.Next('!','z'));
            var newStorage = new Storage()
            {
                StorageName = pass.ToString(),
                MaxSize = 10485760,
                CurrentSize = 0,
                Files = null,
                User = user
            };

            _storageService.AppendStore(newStorage);

            TempData["SuccessForUser"] = String.Format("Вам предоставлено хранилище объемом: {0} Мб.",newStorage.MaxSize);
            return RedirectToAction("Index");
        }


        [HttpGet]
        [Authorize]
        public ActionResult DeleteFile(int id)
        {
            List<Part> parts = _partRepository.GetMany(x=>x.Files.FilesID == id).ToList();
            foreach(var item in parts)
            {
                System.IO.File.Delete(item.CloudPath);
            }
            _storageService.DeleteFile(id);

            var fileList = new FilesListViewModel();
            fileList.StorageName = String.Empty;

            return RedirectToAction("FilesList", "Storage");
        }
    }
}
