﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Ghost.Domain;
using Ghost.Domain.ViewModel;
using Ghost.Service.Interfaces;

namespace Ghost.Controllers
{
    public class AccountController : Controller
    {
        private readonly ISecutiryService _securityService;
        private readonly IMailSender _mailSender;

        public AccountController(ISecutiryService securityService,IMailSender mailSender)
        {
            this._securityService = securityService;
            this._mailSender = mailSender;
        }

        [HttpGet]
        public ActionResult ForgotPassword()
        {
            if (!User.Identity.IsAuthenticated)
                return View();
            else
                return RedirectToAction("Index", "Profile");
        }

        [HttpPost]
        public ActionResult ForgotPassword(ForgotPasswordModel model)
        {
            if (!ModelState.IsValid)
                return View(model);
            try
            {
                var user = _securityService.GetUserById(model.Email);
                Random rnd = new Random((int)DateTime.Now.Ticks);
                StringBuilder pass = new StringBuilder();
                for(int i = 0; i<7;i++)
                    pass.Append((Char)rnd.Next('!','z'));
                user.Password = CreatePasswordHash(pass.ToString(),user.Salt);
                _securityService.UpdateUser(user);

                string mailBody = String.Format("Уважаемый пользователь, ваш новый пароль от аккаунта Ghost : {0}. \nВвиду того что мы не храним пароль в открытом виде, то вернуть ваш старый пароль не можем. \nВы можете заменит данный пароль в настройках аккаунта.",
                    pass.ToString());
                _mailSender.SendMail(model.Email,"Новый пароль от аккаунта на Ghost", mailBody);

                return RedirectToAction("Signin", "Account");
            }
            catch
            {
                ModelState.AddModelError("","Пользователь с данным email в системе не зарегистрирован.");
                return View(model);
            }
        }

        [HttpGet]
        public ActionResult Signin()
        {
            if (!User.Identity.IsAuthenticated)
            {
                return View();
            }
            else
                return RedirectToAction("Index", "Profile");
        }

        [HttpPost]
        public ActionResult Signin(LogOnModel model)
        {
            if (!ModelState.IsValid)
                return View(model);
            try
            {
                var user = _securityService.GetUserById(model.Email) as User;
                string hashedPassword = CreatePasswordHash(model.Password, user.Salt);
                if (_securityService.ValidateUser(model.Email, hashedPassword))
                {
                    FormsAuthentication.SetAuthCookie(user.UserId.ToString(), model.RememberMe);
                }
                else
                {
                    ModelState.AddModelError("", "Пара логин/пароль не верна или аккаунт не активирован.");
                    return View(model);
                }
                FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(1, user.UserId.ToString(), DateTime.Now, DateTime.Now.AddMinutes(1), model.RememberMe, user.Role.RoleName);
                string strEncrypted = FormsAuthentication.Encrypt(ticket);
                Response.Cookies.Add(new HttpCookie("Ghost_A_Osmokiesku",strEncrypted));

                user.DateLastLogin = DateTime.Now;
                _securityService.UpdateUser(user);

                return RedirectToAction("Index", "Profile");
            }
            catch
            {
                ModelState.AddModelError("","Пара логин/пароль не верна.");
                return View(model);
            }
        }

        [HttpGet]
        public ActionResult Signup()
        {
            if (!User.Identity.IsAuthenticated)
            {
                return View();
            }
            else
                return RedirectToAction("Index", "Profile");
        }

        [HttpPost]
        public ActionResult Signup(RegisterModel model)
        {
            if (!ModelState.IsValid)
                return View(model);
            try
            {
                if(!_securityService.IsUserExist(model.Email))
                {
                    Ghost.Domain.User newUser = new User();
                    newUser.Email = model.Email;
                    newUser.Salt = _securityService.CreateSalt();
                    newUser.Password = CreatePasswordHash(model.Password, newUser.Salt);
                    newUser.IsApproved = false;
                    newUser.DateCreated = DateTime.Now;
                    newUser.DateLastLogin = DateTime.Now;
                    newUser.BirthDay = new DateTime(1900, 1, 1, 0, 0, 0);
                    newUser.GenderID = 3;
                    _securityService.CreateUser(newUser);

                    string verifyUrl = System.Web.HttpContext.Current.Request.Url.GetComponents(UriComponents.Host,UriFormat.SafeUnescaped) + "/Account/Activate?email=" + model.Email + "&key=" + newUser.Password;
                    _mailSender.SendMail(model.Email, "Активация аккаунта на Ghost", String.Format("Добро пожаловатьв Ghost! \nДля использования вашего аккаунта необходимо его активировать перейдя по данной ссылке: {0}",verifyUrl));

                    return RedirectToAction("Signin","Account");
                }
                else
                {
                    ModelState.AddModelError("","Данный E-mail уже зарегистрирован в системе.");
                    return View(model);
                }
            }
            catch
            {
                ModelState.AddModelError("", "Произошла внутренняя ошибка, повторите регистрацию еще раз.");
                return View(model);
            }
        }

        [HttpGet]
        public ActionResult SignOut()
        {
            FormsAuthentication.SignOut();
            if (Request.Cookies["Ghost_A_Osmokiesku"] != null)
            {
                var c = new HttpCookie("Ghost_A_Osmokiesku");
                c.Expires = DateTime.Now.AddDays(-1);
                Response.Cookies.Add(c);
            }
            return RedirectToAction("Index", "Promo");
        }

        [HttpGet]
        public ActionResult Activate()
        {
            string email = Request.Params["email"];
            string key = Request.Params["key"];
            var user = _securityService.GetUserById(email);
            if (user != null)
            {
                if (user.Password == key)
                {
                    ViewBag.ActiveMessage = "Учетная запись активирована.";
                    user.IsApproved = true;
                    _securityService.UpdateUser(user);
                }
                else
                    ViewBag.ActiveMessage = "Ключ проверки не подходит.";
            }
            else
                ViewBag.ActiveMessage = "Пользователя с таким логином не существует.";
            return View();
        }

        private static string CreatePasswordHash(string pwd, string salt)
        {
            return FormsAuthentication.HashPasswordForStoringInConfigFile(String.Concat(pwd, salt), "md5");
        }


    }
}