﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ghost.Domain;
using Ghost.Domain.ViewModel;
using Ghost.Service.Interfaces;

namespace Ghost.Controllers
{
    public class StaticController : Controller
    {
        private readonly ISecutiryService _securityService;

        public StaticController(ISecutiryService securityService)
        {
            this._securityService = securityService;
        }

        [HttpGet]
        [Authorize]
        public ActionResult Contact()
        {
            return View();
        }

        [HttpGet]
        [Authorize]
        public ActionResult AboutUs()
        {
            return View();
        }

        [ChildActionOnly]
        public PartialViewResult _LogOnPartial()
        {
            var user = _securityService.GetUserById(Int32.Parse(User.Identity.Name));
            LayoutViewModel userInfo = new LayoutViewModel();
            userInfo.FitstAndLastName = String.Concat(user.FirstName," ",user.LastName);
            userInfo.LastLogOn = user.DateLastLogin.ToString();
            userInfo.AvatarURL = user.AvatarURL;

            return PartialView("_LogOnPartial",userInfo);
        }
    }
}