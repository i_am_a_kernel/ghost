﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Microsoft.Practices.Unity;
using System.Web.Security;
using System.Data.Entity;
using System.Security.Principal;
using Ghost.Data.Infrastructure;
using Ghost.Data.Repositories;
using Ghost.Service.Interfaces;
using Ghost.Service.Services;
using Ghost.IoC;


namespace Ghost
{
    // Примечание: Инструкции по включению классического режима IIS6 или IIS7 
    // см. по ссылке http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            //AuthConfig.RegisterAuth();


            IUnityContainer container = GetUnityContainer();
            DependencyResolver.SetResolver(new UnityDependencyResolver(container));
        }

        private IUnityContainer GetUnityContainer()
        {
            IUnityContainer container = new UnityContainer()
            .RegisterType<IDatabaseFactory, DatabaseFactory>(new HttpContextLifetimeManager<IDatabaseFactory>())
            .RegisterType<IUnitOfWork, UnitOfWork>(new HttpContextLifetimeManager<IUnitOfWork>())
            .RegisterType<ISecutiryService, SecurityService>(new HttpContextLifetimeManager<ISecutiryService>())
            .RegisterType<IMailSender, MailSender>(new HttpContextLifetimeManager<IMailSender>())
            .RegisterType<IImageService, ImageService>(new HttpContextLifetimeManager<IImageService>())
            .RegisterType<IUserService, UserService>(new HttpContextLifetimeManager<IUserService>())
            .RegisterType<IStorageService, StorageService>(new HttpContextLifetimeManager<IStorageService>())
            .RegisterType<ISteganographyService, SteganographyService>(new HttpContextLifetimeManager<ISteganographyService>())
            .RegisterType<IUserRepository, UserRepository>(new HttpContextLifetimeManager<IUserRepository>())
            .RegisterType<IRoleRepository, RoleRepository>(new HttpContextLifetimeManager<IRoleRepository>())
            .RegisterType<IGenderRepository, GenderRepository>(new HttpContextLifetimeManager<IGenderRepository>())
            .RegisterType<IStorageRepository,StorageRepository>(new HttpContextLifetimeManager<IStorageRepository>())
            .RegisterType<IFileRepository,FileRepository>(new HttpContextLifetimeManager<IFileRepository>())
            .RegisterType<IPartRepository,PartRepository>(new HttpContextLifetimeManager<IPartRepository>());
            return container;
        }


        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {
            string cookieName = "Ghost_A_Osmokiesku";
            HttpCookie authCookie = Context.Request.Cookies[cookieName];

            if (null == authCookie)
                return;

            FormsAuthenticationTicket authTicket = null;
            try
            {
                authTicket = FormsAuthentication.Decrypt(authCookie.Value);
            }
            catch
            {
                return;
            }

            if (null == authTicket)
                return;

            string[] roles = authTicket.UserData.Split(new char[] { '|' });
            FormsIdentity id = new FormsIdentity(authTicket);
            GenericPrincipal principal = new GenericPrincipal(id, roles);
            Context.User = principal;
        }
    }
}