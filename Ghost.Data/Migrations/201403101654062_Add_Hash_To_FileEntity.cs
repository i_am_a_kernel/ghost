namespace Ghost.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_Hash_To_FileEntity : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Files", "MD5Hash", c => c.String());
            AddColumn("dbo.Files", "SHA1Hash", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Files", "SHA1Hash");
            DropColumn("dbo.Files", "MD5Hash");
        }
    }
}
