namespace Ghost.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class fix_Disk_Quota : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Files", "FileSize", c => c.Long(nullable: false));
            AlterColumn("dbo.Storages", "MaxSize", c => c.Long(nullable: false));
            AlterColumn("dbo.Storages", "CurrentSize", c => c.Long(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Storages", "CurrentSize", c => c.Double(nullable: false));
            AlterColumn("dbo.Storages", "MaxSize", c => c.Double(nullable: false));
            AlterColumn("dbo.Files", "FileSize", c => c.Double(nullable: false));
        }
    }
}
