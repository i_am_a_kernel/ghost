namespace Ghost.Data.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Ghost.Data.GlobalContex>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
        }

        protected override void Seed(Ghost.Data.GlobalContex context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //

            context.Roles.AddOrUpdate(
                p => p.RoleName,
                new Domain.Role { RoleId = 1, RoleName = "Admin" },
                new Domain.Role { RoleId = 2, RoleName = "Moderator" },
                new Domain.Role { RoleId = 3, RoleName = "User" }
                );

            context.Genders.AddOrUpdate(
                p => p.GenderName,
                new Domain.Gender { GenderID = 1, GenderName = "�������" },
                new Domain.Gender { GenderID = 2, GenderName = "�������" },
                new Domain.Gender { GenderID = 3, GenderName = "�� ������"}
                );
        }
    }
}
