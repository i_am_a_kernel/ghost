namespace Ghost.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddFileFullName_to_FileEntity : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Files", "FileFullName", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Files", "FileFullName");
        }
    }
}
