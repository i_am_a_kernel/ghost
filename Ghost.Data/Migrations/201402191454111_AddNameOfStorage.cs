namespace Ghost.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddNameOfStorage : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Parts", "Storage_StorageId", "dbo.Storages");
            DropIndex("dbo.Parts", new[] { "Storage_StorageId" });
            AddColumn("dbo.Files", "Storage_StorageId", c => c.Int());
            AddColumn("dbo.Storages", "StorageName", c => c.String());
            CreateIndex("dbo.Files", "Storage_StorageId");
            AddForeignKey("dbo.Files", "Storage_StorageId", "dbo.Storages", "StorageId");
            DropColumn("dbo.Parts", "Storage_StorageId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Parts", "Storage_StorageId", c => c.Int());
            DropForeignKey("dbo.Files", "Storage_StorageId", "dbo.Storages");
            DropIndex("dbo.Files", new[] { "Storage_StorageId" });
            DropColumn("dbo.Storages", "StorageName");
            DropColumn("dbo.Files", "Storage_StorageId");
            CreateIndex("dbo.Parts", "Storage_StorageId");
            AddForeignKey("dbo.Parts", "Storage_StorageId", "dbo.Storages", "StorageId");
        }
    }
}
