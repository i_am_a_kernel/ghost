namespace Ghost.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updateIndexes : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Alerts",
                c => new
                    {
                        AlertId = c.Int(nullable: false, identity: true),
                        Subject = c.String(),
                        Body = c.String(),
                        DateTime = c.DateTime(nullable: false),
                        User_UserId = c.Int(),
                    })
                .PrimaryKey(t => t.AlertId)
                .ForeignKey("dbo.Users", t => t.User_UserId)
                .Index(t => t.User_UserId);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        UserId = c.Int(nullable: false, identity: true),
                        Email = c.String(nullable: false),
                        Password = c.String(nullable: false),
                        Salt = c.String(),
                        FirstName = c.String(),
                        LastName = c.String(),
                        AvatarURL = c.String(),
                        IsApproved = c.Boolean(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        DateLastLogin = c.DateTime(nullable: false),
                        Role_RoleId = c.Int(),
                    })
                .PrimaryKey(t => t.UserId)
                .ForeignKey("dbo.Roles", t => t.Role_RoleId)
                .Index(t => t.Role_RoleId);
            
            CreateTable(
                "dbo.Backups",
                c => new
                    {
                        BackupId = c.Int(nullable: false, identity: true),
                        DateOfCreate = c.DateTime(nullable: false),
                        Location = c.String(),
                        User_UserId = c.Int(),
                    })
                .PrimaryKey(t => t.BackupId)
                .ForeignKey("dbo.Users", t => t.User_UserId)
                .Index(t => t.User_UserId);
            
            CreateTable(
                "dbo.Files",
                c => new
                    {
                        FilesID = c.Int(nullable: false, identity: true),
                        LocalPath = c.String(),
                        CloudPath = c.String(),
                        LastChange = c.DateTime(nullable: false),
                        UploadDate = c.DateTime(nullable: false),
                        FileSize = c.Double(nullable: false),
                        Backup_BackupId = c.Int(),
                        User_UserId = c.Int(),
                    })
                .PrimaryKey(t => t.FilesID)
                .ForeignKey("dbo.Backups", t => t.Backup_BackupId)
                .ForeignKey("dbo.Users", t => t.User_UserId)
                .Index(t => t.Backup_BackupId)
                .Index(t => t.User_UserId);
            
            CreateTable(
                "dbo.Parts",
                c => new
                    {
                        PartId = c.Int(nullable: false, identity: true),
                        PartName = c.String(),
                        Extention = c.String(),
                        Backup_BackupId = c.Int(),
                        Storage_StorageId = c.Int(),
                    })
                .PrimaryKey(t => t.PartId)
                .ForeignKey("dbo.Backups", t => t.Backup_BackupId)
                .ForeignKey("dbo.Storages", t => t.Storage_StorageId)
                .Index(t => t.Backup_BackupId)
                .Index(t => t.Storage_StorageId);
            
            CreateTable(
                "dbo.Storages",
                c => new
                    {
                        StorageId = c.Int(nullable: false, identity: true),
                        MaxSize = c.Double(nullable: false),
                        CurrentSize = c.Double(nullable: false),
                        UserId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.StorageId)
                .ForeignKey("dbo.Users", t => t.UserId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.Roles",
                c => new
                    {
                        RoleId = c.Int(nullable: false, identity: true),
                        RoleName = c.String(),
                    })
                .PrimaryKey(t => t.RoleId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Users", "Role_RoleId", "dbo.Roles");
            DropForeignKey("dbo.Backups", "User_UserId", "dbo.Users");
            DropForeignKey("dbo.Storages", "UserId", "dbo.Users");
            DropForeignKey("dbo.Parts", "Storage_StorageId", "dbo.Storages");
            DropForeignKey("dbo.Parts", "Backup_BackupId", "dbo.Backups");
            DropForeignKey("dbo.Files", "User_UserId", "dbo.Users");
            DropForeignKey("dbo.Files", "Backup_BackupId", "dbo.Backups");
            DropForeignKey("dbo.Alerts", "User_UserId", "dbo.Users");
            DropIndex("dbo.Users", new[] { "Role_RoleId" });
            DropIndex("dbo.Backups", new[] { "User_UserId" });
            DropIndex("dbo.Storages", new[] { "UserId" });
            DropIndex("dbo.Parts", new[] { "Storage_StorageId" });
            DropIndex("dbo.Parts", new[] { "Backup_BackupId" });
            DropIndex("dbo.Files", new[] { "User_UserId" });
            DropIndex("dbo.Files", new[] { "Backup_BackupId" });
            DropIndex("dbo.Alerts", new[] { "User_UserId" });
            DropTable("dbo.Roles");
            DropTable("dbo.Storages");
            DropTable("dbo.Parts");
            DropTable("dbo.Files");
            DropTable("dbo.Backups");
            DropTable("dbo.Users");
            DropTable("dbo.Alerts");
        }
    }
}
