namespace Ghost.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class fixEnablingForeignKeyProperties : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Users", "Gender_GenderID", "dbo.Genders");
            DropIndex("dbo.Users", new[] { "Gender_GenderID" });
            RenameColumn(table: "dbo.Users", name: "Gender_GenderID", newName: "GenderID");
            AlterColumn("dbo.Users", "GenderID", c => c.Int(nullable: false, defaultValue: 3));
            CreateIndex("dbo.Users", "GenderID");
            AddForeignKey("dbo.Users", "GenderID", "dbo.Genders", "GenderID", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Users", "GenderID", "dbo.Genders");
            DropIndex("dbo.Users", new[] { "GenderID" });
            AlterColumn("dbo.Users", "GenderID", c => c.Int());
            RenameColumn(table: "dbo.Users", name: "GenderID", newName: "Gender_GenderID");
            CreateIndex("dbo.Users", "Gender_GenderID");
            AddForeignKey("dbo.Users", "Gender_GenderID", "dbo.Genders", "GenderID");
        }
    }
}
