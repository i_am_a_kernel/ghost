namespace Ghost.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Delete_Backup_Entry : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Files", "Backup_BackupId", "dbo.Backups");
            DropForeignKey("dbo.Parts", "Backup_BackupId", "dbo.Backups");
            DropForeignKey("dbo.Backups", "User_UserId", "dbo.Users");
            DropIndex("dbo.Files", new[] { "Backup_BackupId" });
            DropIndex("dbo.Parts", new[] { "Backup_BackupId" });
            DropIndex("dbo.Backups", new[] { "User_UserId" });
            AddColumn("dbo.Parts", "CloudPath", c => c.String());
            AddColumn("dbo.Parts", "Files_FilesID", c => c.Int());
            CreateIndex("dbo.Parts", "Files_FilesID");
            AddForeignKey("dbo.Parts", "Files_FilesID", "dbo.Files", "FilesID");
            DropColumn("dbo.Files", "LocalPath");
            DropColumn("dbo.Files", "CloudPath");
            DropColumn("dbo.Files", "LastChange");
            DropColumn("dbo.Files", "Backup_BackupId");
            DropColumn("dbo.Parts", "Backup_BackupId");
            DropTable("dbo.Backups");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Backups",
                c => new
                    {
                        BackupId = c.Int(nullable: false, identity: true),
                        DateOfCreate = c.DateTime(nullable: false),
                        Location = c.String(),
                        User_UserId = c.Int(),
                    })
                .PrimaryKey(t => t.BackupId);
            
            AddColumn("dbo.Parts", "Backup_BackupId", c => c.Int());
            AddColumn("dbo.Files", "Backup_BackupId", c => c.Int());
            AddColumn("dbo.Files", "LastChange", c => c.DateTime(nullable: false));
            AddColumn("dbo.Files", "CloudPath", c => c.String());
            AddColumn("dbo.Files", "LocalPath", c => c.String());
            DropForeignKey("dbo.Parts", "Files_FilesID", "dbo.Files");
            DropIndex("dbo.Parts", new[] { "Files_FilesID" });
            DropColumn("dbo.Parts", "Files_FilesID");
            DropColumn("dbo.Parts", "CloudPath");
            CreateIndex("dbo.Backups", "User_UserId");
            CreateIndex("dbo.Parts", "Backup_BackupId");
            CreateIndex("dbo.Files", "Backup_BackupId");
            AddForeignKey("dbo.Backups", "User_UserId", "dbo.Users", "UserId");
            AddForeignKey("dbo.Parts", "Backup_BackupId", "dbo.Backups", "BackupId");
            AddForeignKey("dbo.Files", "Backup_BackupId", "dbo.Backups", "BackupId");
        }
    }
}
