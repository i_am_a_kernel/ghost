namespace Ghost.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateUserModel : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Genders",
                c => new
                    {
                        GenderID = c.Int(nullable: false, identity: true),
                        GenderName = c.String(),
                    })
                .PrimaryKey(t => t.GenderID);
            
            AddColumn("dbo.Users", "BirthDay", c => c.DateTime(nullable: false));
            AddColumn("dbo.Users", "PhoneNumber", c => c.String());
            AddColumn("dbo.Users", "AboutMe", c => c.String());
            AddColumn("dbo.Users", "Gender_GenderID", c => c.Int());
            CreateIndex("dbo.Users", "Gender_GenderID");
            AddForeignKey("dbo.Users", "Gender_GenderID", "dbo.Genders", "GenderID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Users", "Gender_GenderID", "dbo.Genders");
            DropIndex("dbo.Users", new[] { "Gender_GenderID" });
            DropColumn("dbo.Users", "Gender_GenderID");
            DropColumn("dbo.Users", "AboutMe");
            DropColumn("dbo.Users", "PhoneNumber");
            DropColumn("dbo.Users", "BirthDay");
            DropTable("dbo.Genders");
        }
    }
}
