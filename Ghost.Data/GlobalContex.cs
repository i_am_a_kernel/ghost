﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using Ghost.Domain;

namespace Ghost.Data
{
    public class GlobalContex : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Gender> Genders { get; set; }
        public DbSet<Alert> Alerts { get; set; }
        public DbSet<Files> Files { get; set; }
        public DbSet<Part> Parts { get; set; }
        public DbSet<Storage> Storages { get; set; }

        public GlobalContex()
            : base("GlobalContext")
        {
        }

        public virtual void Commit()
        {
            base.SaveChanges();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<GlobalContex, Ghost.Data.Migrations.Configuration>());
            modelBuilder.Entity<User>();

            modelBuilder.Entity<Storage>()
                .HasRequired(t => t.User)
                .WithOptional(t => t.Storage)
                .Map(t => t.MapKey("UserId"));
        }
    }
}
