﻿using System;
using System.Collections.Generic;
using Ghost.Domain;
using Ghost.Data.Infrastructure;

namespace Ghost.Data.Repositories
{
    public class GenderRepository : GenericRepository<Gender>, IGenderRepository
    {
        public GenderRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {
        }
    }

    public interface IGenderRepository : IGenericRepository<Gender>
    {
    }
}
