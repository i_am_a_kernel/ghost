﻿using System;
using System.Collections.Generic;
using Ghost.Domain;
using Ghost.Data.Infrastructure;

namespace Ghost.Data.Repositories
{
    public class UserRepository : GenericRepository<User>, IUserRepository
    {
        public UserRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {
        }

        public void AssignRole(int userId, int  roleId)
        {
            var user = this.GetById(userId);
            user.Role = null;

            var role = this.DataContext.Roles.Find(roleId);
            user.Role = role;

            this.DataContext.Users.Attach(user);
            this.DataContext.Entry(user).State = System.Data.Entity.EntityState.Modified;
        }

        public Role UserInRole(int userId)
        {
            var roleOfUser = this.GetById(userId).Role;
            return roleOfUser;
        }

        public bool UserIsInRole(int userId, int roleId)
        {
            var user = this.GetById(userId);
            var role = this.DataContext.Roles.Find(roleId);

            if (user.Role.Equals(role))
                return true;
            else
                return false;
        }

        public void AssignGender(int userId, int genderId)
        {
            var user = this.GetById(userId);
            user.Gender = null;

            var gender = this.DataContext.Genders.Find(genderId);
            user.Gender = gender;

            this.DataContext.Users.Attach(user);
            this.DataContext.Entry(user).State = System.Data.Entity.EntityState.Modified;
        }
    }

    public interface IUserRepository : IGenericRepository<User>
    {
        void AssignRole(int userId, int roleId);
        Role UserInRole(int userId);
        bool UserIsInRole(int userId, int roleId);
        void AssignGender(int userId, int genderId);
    }
}
