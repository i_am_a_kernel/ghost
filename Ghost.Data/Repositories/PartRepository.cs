﻿using System;
using System.Collections.Generic;
using Ghost.Domain;
using Ghost.Data.Infrastructure;

namespace Ghost.Data.Repositories
{
    public class PartRepository : GenericRepository<Part>, IPartRepository
    {
        public PartRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {
        }
    }

    public interface IPartRepository : IGenericRepository<Part>
    {
    }
}
