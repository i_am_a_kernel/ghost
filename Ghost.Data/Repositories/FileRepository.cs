﻿using System;
using System.Collections.Generic;
using Ghost.Domain;
using Ghost.Data.Infrastructure;

namespace Ghost.Data.Repositories
{
    public class FileRepository : GenericRepository<Files>, IFileRepository
    {
        public FileRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {
        }
    }

    public interface IFileRepository : IGenericRepository<Files>
    {
    }
}
