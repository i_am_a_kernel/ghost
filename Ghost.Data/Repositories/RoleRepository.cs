﻿using System;
using System.Collections.Generic;
using Ghost.Domain;
using Ghost.Data.Infrastructure;

namespace Ghost.Data.Repositories
{
    public class RoleRepository : GenericRepository<Role>, IRoleRepository
    {
        public RoleRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        { 
        }
    }

    public interface IRoleRepository : IGenericRepository<Role>
    {
    }
}
