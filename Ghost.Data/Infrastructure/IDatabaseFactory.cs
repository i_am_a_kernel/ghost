﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace Ghost.Data.Infrastructure
{
    public interface IDatabaseFactory : IDisposable
    {
        GlobalContex Get();
    }
}
