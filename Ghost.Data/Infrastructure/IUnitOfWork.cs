﻿using System;
using System.Collections.Generic;

namespace Ghost.Data.Infrastructure
{
    public interface IUnitOfWork
    {
        void Commit();
    }
}
