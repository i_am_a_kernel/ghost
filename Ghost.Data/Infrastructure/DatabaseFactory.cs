﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using Ghost.Domain;

namespace Ghost.Data.Infrastructure
{
    public class DatabaseFactory : Disposable, IDatabaseFactory
    {
        public DatabaseFactory()
        {
            
        }

        private GlobalContex dataContext;

        public GlobalContex Get()
        {
            return dataContext ?? (dataContext = new GlobalContex());
        }

        protected override void DisposeCore()
        {
            if (dataContext != null)
                dataContext.Dispose();
        }
    }
}
