﻿using System;
using System.Collections.Generic;

namespace Ghost.Data.Infrastructure
{
    public interface IGenericRepository<T> where T : class
    {
        void Add(T entity);
        void Update(T entity);
        void Delete(T entity);
        void Delete(Func<T, bool> predicate);
        T GetById(long Id);
        T Get(Func<T, bool> where);
        IEnumerable<T> GetAll();
        IEnumerable<T> GetMany(Func<T, bool> where);
    }
}
