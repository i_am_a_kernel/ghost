﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace Ghost.Domain.ViewModel
{
    public class ProfileViewModel
    {
        public string FirstName { get; set; }
        
        public string LastName { get; set; }

        [DataType(DataType.MultilineText)]
        public string AboutMe { get; set; }
        
        [DataType(DataType.Date)]
        public DateTime BirthDay { get; set; }

        [DataType(DataType.PhoneNumber)]
        public string PhoneNumber { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [DataType(DataType.Password)]
        public string OldPassword { get; set; }

        [DataType(DataType.Password)]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        public string ConfirmNewPassword { get; set; }
        
        public string AvatarUrl { get; set; }

        [DataType(DataType.DateTime)]
        public DateTime DateCreated { get; set; }

        [DataType(DataType.DateTime)]
        public DateTime DateLastLogin { get; set; }

        public int GenderID { get; set; }

        public int FileCounter { get; set; }

        public int StorageCounter { get; set; }

        public SmallStorageData StorageData { get; set; }

    }

    public class SmallStorageData
    {
        public double TotalSize { get; set; }

        public double CurentSize { get; set; }

        public double FreeSize { get; set; }
    }
}
