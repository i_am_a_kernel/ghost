﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Ghost.Domain.ViewModel
{
    public class LogOnModel
    {
        [Required]
        [DataType(DataType.EmailAddress,ErrorMessage="Введите верный email")]
        [Display(Name = "Email пользователя")]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Пароль")]
        public string Password { get; set; }

        [Display(Name = "Запомнить меня")]
        public bool RememberMe { get; set; }
    }

    public class RegisterModel
    {
        [Required]
        [DataType(DataType.EmailAddress, ErrorMessage = "Введите верный email")]
        [Display(Name = "Email пользователя")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "Значение \"{0}\" должно содержать не менее {2} символов.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Пароль")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Подтверждение пароля")]
        [Compare("Password", ErrorMessage = "Пароль и его подтверждение не совпадают.")]
        public string ConfirmPassword { get; set; }
    }

    public class ForgotPasswordModel
    {
        [Required]
        [DataType(DataType.EmailAddress, ErrorMessage = "Введите верный email")]
        [Display(Name = "Email пользователя")]
        public string Email { get; set; }
    }
}
