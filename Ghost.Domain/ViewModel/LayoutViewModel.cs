﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Ghost.Domain.ViewModel
{
    public class LayoutViewModel
    {
        [DataType(DataType.DateTime)]
        public string LastLogOn { get; set; }

        public string FitstAndLastName { get; set; }

        public string AvatarURL { get; set; }
    }
}
