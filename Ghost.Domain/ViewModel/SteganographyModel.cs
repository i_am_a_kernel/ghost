﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Ghost.Domain.ViewModel
{
    public class SteganographyModel
    {
        public int Layer { get; set; }
        public bool writeFileName { get; set; }
        public long fileSize { get; set; }
        public string fileName { get; set; }
        public long fileNameSize { get; set; }
    }
}
