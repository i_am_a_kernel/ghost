﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using PagedList;

namespace Ghost.Domain.ViewModel
{
    public class UploadFileViewModel
    {
        [Required]
        public int StorageId { get; set; }
    }

    public class DeleteStorageViewMode
    {
        public Storage CurentStorage { get; set; }

        [Required]
        public string ValidationKey { get; set; }

        public string CoreValidationKey { get; set; }

        public int StorageId { get; set; }
    }

    public class FilesListViewModel
    {
        public IPagedList<Files> Files { get; set; }

        public string  StorageName { get; set; }

        public int StorageId { get; set; }
    }

    public class PartialFilesListViewModel
    {
        public List<Files> Files { get; set; }
    }

}
