﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Ghost.Domain
{
    public class Files
    {
        [Key]
        public int FilesID { get; set; }

        public DateTime UploadDate { get; set; }

        public string FileFullName { get; set; }

        public long FileSize { get; set; }

        public string MD5Hash { get; set; }

        public string SHA1Hash { get; set; }

        public virtual User User { get; set; }
        public virtual Storage Storage { get; set; }
        public virtual ICollection<Part> Part { get; set; }
    }
}
