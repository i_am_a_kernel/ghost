﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace Ghost.Domain
{
    public class User
    {
        [Key]
        public int UserId { get; set; }
        public int GenderID { get; set; }

        [Required]
        [Display(Name = "E-mail")]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        public string Salt { get; set; }

        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [DataType(DataType.Date)]
        [Display(Name="Birth day")]
        public DateTime BirthDay { get; set; }

        [DataType(DataType.PhoneNumber)]
        [Display(Name="Phone Number")]
        public string PhoneNumber { get; set; }

        [DataType(DataType.MultilineText)]
        [Display(Name="About Me")]
        public string AboutMe { get; set; }

        public string AvatarURL { get; set; }

        [Display(Name="Approved?")]
        public bool IsApproved { get; set; }

        [DataType(DataType.DateTime)]
        [Display(Name = "Date Created")]
        public DateTime DateCreated { get; set; }

        [DataType(DataType.DateTime)]
        [Display(Name = "Last Login Date")]
        public DateTime DateLastLogin { get; set; }

        public virtual Role Role { get; set; }
        public virtual Gender Gender { get; set; }
        public virtual ICollection<Alert> Alert { get; set; }
        public virtual ICollection<Files> Files { get; set; }
        public virtual Storage Storage { get; set; }
    }
}
