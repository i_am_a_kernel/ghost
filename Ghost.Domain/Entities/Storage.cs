﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace Ghost.Domain
{
    public class Storage
    {
        [Key]
        public int StorageId { get; set; }

        [Display(Name="Название хранилища")]
        public string StorageName { get; set; }

        [Display(Name = "Максимальный объем")]
        public long MaxSize { get; set; }

        [Display(Name = "Занято")]
        public long CurrentSize { get; set; }

        public virtual User User { get; set; }
        public virtual ICollection<Files> Files { get; set; }
    }
}
