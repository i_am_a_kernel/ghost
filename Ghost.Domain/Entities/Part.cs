﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace Ghost.Domain
{
    public class Part
    {
        [Key]
        public int PartId { get; set; }

        public string PartName { get; set; }

        public string CloudPath { get; set; }

        public string Extention { get; set; }

        public virtual Files Files { get; set; }
    }
}
