﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace Ghost.Domain
{
    public class Gender
    {
        [Key]
        public int GenderID { get; set; }

        public string GenderName { get; set; }

        public virtual ICollection<User> User { get; set; }
    }
}
