﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace Ghost.Domain
{
    public class Alert
    {
        [Key]
        public int AlertId { get; set; }

        [Display(Name = "Subject")]
        public string Subject { get; set; }

        [Display(Name = "Alert Message")]
        public string Body { get; set; }

        [Display(Name = "Date Created")]
        public DateTime DateTime { get; set; }

        public virtual User User { get; set; }
    }
}
