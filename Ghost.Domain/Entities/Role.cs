﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace Ghost.Domain
{
    public class Role
    {
        [Key]
        public int RoleId { get; set; }

        [Display(Name="")]
        public string RoleName { get; set; }

        public virtual ICollection<User> User { get; set; }
    }
}
