﻿using System;
using System.Collections.Generic;
using Ghost.Domain;

namespace Ghost.Service.Interfaces
{
    public interface IStorageService
    {
        IEnumerable<Storage> GetAllStorages();
        IEnumerable<Storage> GetStoragesOfUser(int userId);
        Storage GetStorageById(int id);
        void AppendStore(Storage storage);
        void DeleteStorage(int id);
        void DeleteFile(int id);

        void SaveStorage();
    }
}
