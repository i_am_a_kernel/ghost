﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ghost.Domain;

namespace Ghost.Service.Interfaces
{
    public interface ISecutiryService
    {
        IEnumerable<User> GetUsers();
        User GetUserById(int id);
        User GetUserById(string email);
        void CreateUser(User user);
        void UpdateUser(User user);
        void DeleteUser(int id);

        void AssignRole(int userId, int roleId);
        Role UserInRole(int userId);
        bool UserIsInRole(int userId, int roleId);
        bool IsUserExist(string email);
        bool ValidateUser(string email, string hashedPassword);
        string CreateSalt();

        void SaveUser();
    }
}
