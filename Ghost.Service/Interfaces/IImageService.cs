﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;
using System.Collections;

namespace Ghost.Service.Interfaces
{
    public interface IImageService
    {
        Image ScaleImage(Image src, int size);
        ArrayList GetRandomImages();
    }
}
