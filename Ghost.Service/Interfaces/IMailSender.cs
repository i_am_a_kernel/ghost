﻿using System;
using System.Collections.Generic;

namespace Ghost.Service.Interfaces
{
    public interface IMailSender
    {
        void SendMail(string To, string Subject, string Body);
    }
}
