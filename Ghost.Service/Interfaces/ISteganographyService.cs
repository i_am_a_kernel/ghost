﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Ghost.Domain.ViewModel;

namespace Ghost.Service.Interfaces
{
    public interface ISteganographyService
    {
        void UploadFile(byte[] uploadFile, SteganographyModel sModel, int StorageId);

        byte[] DownloadFile(int fileId);

        string GetRandomKey(int length);
    }
}
