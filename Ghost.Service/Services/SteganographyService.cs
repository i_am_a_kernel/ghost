﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.IO;
using System.IO.Compression;
using Ghost.Data.Infrastructure;
using Ghost.Data.Repositories;
using Ghost.Domain;
using Ghost.Service.Interfaces;
using Ghost.Domain.ViewModel;
using System.Security.Cryptography;
using System.Net;
using System.Text;

namespace Ghost.Service.Services
{
    public class SteganographyService : ISteganographyService
    {
        private readonly IStorageRepository _storageRepository;
        private readonly IStorageService _storageService;
        private readonly IFileRepository _fileRepository;
        private readonly IImageService _imageService;
        private readonly IPartRepository _partRepository;
        private readonly IUnitOfWork _unitOfWork;

        public SteganographyService(IStorageRepository storageRepository
            ,IStorageService storageService
            ,IFileRepository fileRepository
            ,IImageService imageService
            ,IPartRepository partRepository
            ,IUnitOfWork unitOfWork)
        {
            this._storageRepository = storageRepository;
            this._storageService = storageService;
            this._fileRepository = fileRepository;
            this._partRepository = partRepository;
            this._imageService = imageService;
            this._unitOfWork = unitOfWork;
        }

        public void UploadFile(byte[] uploadFile,SteganographyModel sModel, int storageId)
        {
            var storage = _storageRepository.GetById(storageId);
            //Проверяем, что в хранилище достаточно места.
            if ((storage.MaxSize - storage.CurrentSize) <= sModel.fileSize)
            {
                //Исключение "Не хватает места в хранилище для файла"
                return;
            }
            //Арихивация в архив (в потоке)
            // --- Новый метод сжатия (использовать после проверки разбиения)
            /*
            var compressedStream = new MemoryStream();
            var zipStream = new GZipStream(compressedStream,CompressionMode.Compress);
            zipStream.Write(uploadFile, 0, uploadFile.Length);
            zipStream.Close();
            byte[] compressUploadFile = compressedStream.ToArray();
            */
            // --- 

            byte[] compressUploadFile = uploadFile;

            sModel.fileSize = compressUploadFile.Length;
           
            
            //Получаем список контейнеров, выбираем их. Проверка на размер контейнера. Если не хватает дробим.
            var container = _imageService.GetRandomImages();
            int index = 0;
            double[] minCanSave = new double[container.Count];
            List<string> containerOnHost = new List<string>();
            if (!(Directory.Exists(Path.Combine(AppDomain.CurrentDomain.BaseDirectory + "App_Data/Temp_" + storageId.ToString() + "/"))))
                Directory.CreateDirectory(Path.Combine(AppDomain.CurrentDomain.BaseDirectory + "App_Data/Temp_" + storageId.ToString() + "/"));
            foreach(string item in container)
            {
                string pathOnHost = "";
                using (WebClient webClient = new WebClient())
                {
                    pathOnHost = String.Concat(Path.Combine(AppDomain.CurrentDomain.BaseDirectory + "App_Data/Temp_" + storageId.ToString() + "/" + Path.GetFileName(item)));
                    webClient.DownloadFile(new Uri(item),pathOnHost);
                    containerOnHost.Add(pathOnHost);
                }
            }
            foreach(string item in containerOnHost)
            {
                var tempImage = Image.FromFile(item);
                double canSave = (8.0 * ((tempImage.Height * (tempImage.Width / 3) * 3) / 3 - 1)) * 10 / 1024;//* 100 / 1024;
                minCanSave[index] = canSave;
                tempImage.Dispose();
                index++;
            }

            int sizeOfPart = (int)Math.Ceiling(minCanSave.Min());
            int countOfParts = (int)Math.Ceiling((double)sModel.fileSize / sizeOfPart);
            if (countOfParts == 0)
            {
                Directory.Delete(Path.Combine(AppDomain.CurrentDomain.BaseDirectory + "App_Data/Temp_" + storageId.ToString() + "/"), true);
                return;
            }

            //Добавление новых данных в таблицы "ФАЙЛЫ", обновление в "ХРАНИЛИЩЕ"
            storage.CurrentSize += sModel.fileSize;
            _storageRepository.Update(storage);
            _storageService.SaveStorage();
            var md5 = MD5.Create();
            var sha1 = SHA256Managed.Create();

            var newFile = new Files()
            {
                User = storage.User,
                FileSize = sModel.fileSize,
                FileFullName = sModel.fileName,
                Storage = storage,
                UploadDate = DateTime.Now,
                MD5Hash = BitConverter.ToString(md5.ComputeHash(uploadFile)),
                SHA1Hash = BitConverter.ToString(sha1.ComputeHash(uploadFile)),
                Part = null
            };


            if (countOfParts > 1) //Надо бить на части
            {
                // --- Новый метод разбиения!
                MemoryStream msPart;
                int sizeRemaining = (int)compressUploadFile.Length;
                int fileOffset = 0;
                List<MemoryStream> parts = new List<MemoryStream>();
                for(int i = 0; i<countOfParts; i++)
                {
                    msPart = new MemoryStream();
                    sizeRemaining = (int)compressUploadFile.Length - (i * sizeOfPart);

                    if (sizeRemaining < sizeOfPart)
                        sizeOfPart = sizeRemaining;
                    msPart.Write(compressUploadFile, fileOffset, sizeOfPart);
                    parts.Add(msPart);
                    msPart.Close();
                    fileOffset += sizeOfPart;
                }
                // ---

                for(int i = 0;i<parts.Count;i++)
                {
                    string saveToImage = String.Concat(Path.Combine(AppDomain.CurrentDomain.BaseDirectory + "App_Data/" + storage.User.UserId + "/")
                    , System.Guid.NewGuid() + ".png");
                    Random rnd = new Random(unchecked((int)DateTime.Now.Ticks));
                    index = rnd.Next(0, container.Count - 1);
                    string loadedTrueImagePath = containerOnHost[index].ToString();
                    Image loadedTrueImage = Image.FromFile(loadedTrueImagePath);
                    Bitmap loadedTrueBitmap = new Bitmap(loadedTrueImage);

                    sModel.fileSize = parts[i].ToArray().Length;

                    Bitmap changedBitmap = SteganographyHelper.EncryptLayer(loadedTrueBitmap, sModel, parts[i].ToArray());
       
                    using (MemoryStream memory = new MemoryStream())
                    {
                        using (FileStream fs = new FileStream(saveToImage, FileMode.Create, FileAccess.ReadWrite))
                        {
                            changedBitmap.Save(memory, System.Drawing.Imaging.ImageFormat.Png);
                            byte[] bytes = memory.ToArray();
                            fs.Write(bytes, 0, bytes.Length);
                        }

                    }

                    if (i == 0)
                    {
                        _fileRepository.Add(newFile);
                        _unitOfWork.Commit();
                    }
                    Part part = new Part()
                    {
                        Files = newFile,
                        CloudPath = saveToImage,
                        Extention = Path.GetExtension(sModel.fileName),
                        PartName = Path.GetFileNameWithoutExtension(saveToImage)
                    };
                    _partRepository.Add(part);
                    _unitOfWork.Commit();
                    loadedTrueImage.Dispose();
                }
            }
            else // Бить на части не надо, сразу к стеганографии переходим
            {
                string saveToImage = String.Concat(Path.Combine(AppDomain.CurrentDomain.BaseDirectory + "App_Data/" + storage.User.UserId + "/")
                    , System.Guid.NewGuid() + ".png");
                Random rnd = new Random(unchecked((int)DateTime.Now.Ticks));
                index = rnd.Next(0, container.Count - 1);
                string loadedTrueImagePath = containerOnHost[index].ToString();
                Image loadedTrueImage = Image.FromFile(loadedTrueImagePath);
                Bitmap loadedTrueBitmap = new Bitmap(loadedTrueImage);

                Bitmap changedBitmap = SteganographyHelper.EncryptLayer(loadedTrueBitmap, sModel, compressUploadFile);
                using (MemoryStream memory = new MemoryStream())
                {
                    using (FileStream fs = new FileStream(saveToImage, FileMode.Create, FileAccess.ReadWrite))
                    {
                        changedBitmap.Save(memory, System.Drawing.Imaging.ImageFormat.Png);
                        byte[] bytes = memory.ToArray();
                        fs.Write(bytes, 0, bytes.Length);
                    }

                }

                _fileRepository.Add(newFile);
                _unitOfWork.Commit();

                Part part = new Part()
                {
                    Files = newFile,
                    CloudPath = saveToImage,
                    Extention = Path.GetExtension(sModel.fileName),
                    PartName = Path.GetFileNameWithoutExtension(saveToImage)
                };
                _partRepository.Add(part);
                _unitOfWork.Commit();
                loadedTrueImage.Dispose();
            }
            Directory.Delete(Path.Combine(AppDomain.CurrentDomain.BaseDirectory + "App_Data/Temp_" + storageId.ToString() + "/"), true);
        }

        public byte[] DownloadFile(int fileId)
        {
            var file = _fileRepository.GetById(fileId);
            List<byte[]> listOfParts = new List<byte[]>();
            //Извлечение
            var parts = _partRepository.GetMany(x => x.Files.FilesID == fileId);
            foreach (var item in parts)
            {
                string partImagePath = item.CloudPath;
                Image decryptedImage = Image.FromFile(partImagePath);
                int height = decryptedImage.Height;
                int width = decryptedImage.Width;
                Bitmap decryptedBitmap = new Bitmap(decryptedImage);

                byte[] res = SteganographyHelper.DecryptLayer(decryptedBitmap, height, width);
                listOfParts.Add(res);
                decryptedImage.Dispose();
                decryptedBitmap.Dispose();
            }

            // --- Новый метод сборки
            MemoryStream msSource = new MemoryStream();
            foreach (var item in listOfParts)
            {
                msSource.Write(item, 0, item.Length);
            }
            byte[] inputByteArray = msSource.ToArray();
            msSource.Close();

            return inputByteArray;
            // --- Новый метод распаковки (использовать после проверки метода сборки)   
            /*
            var compressedStream = new MemoryStream(inputByteArray);
            var zipStream = new GZipStream(compressedStream, CompressionMode.Decompress);
            var resultStream = new MemoryStream();
            zipStream.CopyTo(resultStream);
            return resultStream.ToArray();
            */
            // --- 
        }

        public string GetRandomKey(int length)
        {
            Random rnd = new Random((int)DateTime.Now.Ticks);
            StringBuilder pass = new StringBuilder();
            for (int i = 0; i < length; i++)
                pass.Append((Char)rnd.Next('!', 'z'));
            return pass.ToString();
        }

    }

    internal static class SteganographyHelper
    {

        public static byte[] DecryptLayer(Bitmap decryptedBitmap, int height, int width)
        {
            int i, j = 0;
            bool[] t = new bool[8];
            bool[] rb = new bool[8];
            bool[] gb = new bool[8];
            bool[] bb = new bool[8];
            Color pixel = new Color();
            byte r, g, b;
            pixel = decryptedBitmap.GetPixel(width - 1, height - 1);
            long fSize = pixel.R + pixel.G * 100 + pixel.B * 10000;
            pixel = decryptedBitmap.GetPixel(width - 2, height - 1);
            long fNameSize = pixel.R + pixel.G * 100 + pixel.B * 10000;
            byte[] res = new byte[fSize];
            string resFName = "";
            byte temp;

            //Read file name:
            for (i = 0; i < height && i * (height / 3) < fNameSize; i++)
                for (j = 0; j < (width / 3) * 3 && i * (height / 3) + (j / 3) < fNameSize; j++)
                {
                    pixel = decryptedBitmap.GetPixel(j, i);
                    r = pixel.R;
                    g = pixel.G;
                    b = pixel.B;
                    byte2bool(r, ref rb);
                    byte2bool(g, ref gb);
                    byte2bool(b, ref bb);
                    if (j % 3 == 0)
                    {
                        t[0] = rb[7];
                        t[1] = gb[7];
                        t[2] = bb[7];
                    }
                    else if (j % 3 == 1)
                    {
                        t[3] = rb[7];
                        t[4] = gb[7];
                        t[5] = bb[7];
                    }
                    else
                    {
                        t[6] = rb[7];
                        t[7] = gb[7];
                        temp = bool2byte(t);
                        resFName += (char)temp;
                    }
                }

            //Read file on layer 8 (after file name):
            int tempj = j;
            i--;

            for (; i < height && i * (height / 3) < fSize + fNameSize; i++)
                for (j = 0; j < (width / 3) * 3 && i * (height / 3) + (j / 3) < (height * (width / 3) * 3) / 3 - 1 && i * (height / 3) + (j / 3) < fSize + fNameSize; j++)
                {
                    if (tempj != 0)
                    {
                        j = tempj;
                        tempj = 0;
                    }
                    pixel = decryptedBitmap.GetPixel(j, i);
                    r = pixel.R;
                    g = pixel.G;
                    b = pixel.B;
                    byte2bool(r, ref rb);
                    byte2bool(g, ref gb);
                    byte2bool(b, ref bb);
                    if (j % 3 == 0)
                    {
                        t[0] = rb[7];
                        t[1] = gb[7];
                        t[2] = bb[7];
                    }
                    else if (j % 3 == 1)
                    {
                        t[3] = rb[7];
                        t[4] = gb[7];
                        t[5] = bb[7];
                    }
                    else
                    {
                        t[6] = rb[7];
                        t[7] = gb[7];
                        temp = bool2byte(t);
                        res[i * (height / 3) + j / 3 - fNameSize] = temp;
                    }
                }

            //Read file on other layers:
            long readedOnL8 = (height * (width / 3) * 3) / 3 - fNameSize - 1;

            for (int layer = 6; layer >= 0 && readedOnL8 + (6 - layer) * ((height * (width / 3) * 3) / 3 - 1) < fSize; layer--)
                for (i = 0; i < height && i * (height / 3) + readedOnL8 + (6 - layer) * ((height * (width / 3) * 3) / 3 - 1) < fSize; i++)
                    for (j = 0; j < (width / 3) * 3 && i * (height / 3) + (j / 3) + readedOnL8 + (6 - layer) * ((height * (width / 3) * 3) / 3 - 1) < fSize; j++)
                    {
                        pixel = decryptedBitmap.GetPixel(j, i);
                        r = pixel.R;
                        g = pixel.G;
                        b = pixel.B;
                        byte2bool(r, ref rb);
                        byte2bool(g, ref gb);
                        byte2bool(b, ref bb);
                        if (j % 3 == 0)
                        {
                            t[0] = rb[layer];
                            t[1] = gb[layer];
                            t[2] = bb[layer];
                        }
                        else if (j % 3 == 1)
                        {
                            t[3] = rb[layer];
                            t[4] = gb[layer];
                            t[5] = bb[layer];
                        }
                        else
                        {
                            t[6] = rb[layer];
                            t[7] = gb[layer];
                            temp = bool2byte(t);
                            res[i * (height / 3) + j / 3 + (6 - layer) * ((height * (width / 3) * 3) / 3 - 1) + readedOnL8] = temp;
                        }
                    }
            return res;
        }


        public static Bitmap EncryptLayer(Bitmap inputBitmap,SteganographyModel model, byte[] fileContainer)
        {
            Bitmap outputBitmap = inputBitmap;
            
            int layer = model.Layer;
            int height = inputBitmap.Height;
            int width = inputBitmap.Width;
            long startPosition = 0;
           //long fileNameSize = JustFName(loadedFilePath).Length;
            long fileNameSize = model.fileNameSize;
            long endPosition = (height * (width / 3) * 3) / 3 - fileNameSize - 1;

            //long fileSize = new FileInfo(loadedFilePath).Length;
            long fileSize = model.fileSize;



            layer--;
            int i = 0, j = 0;
            long FNSize = 0;
            bool[] t = new bool[8];
            bool[] rb = new bool[8];
            bool[] gb = new bool[8];
            bool[] bb = new bool[8];
            Color pixel = new Color();
            byte r, g, b;

            if (model.writeFileName)
            {
                FNSize = fileNameSize;
                //string fileName = JustFName(loadedFilePath);
                string fileName = model.fileName;

                //write fileName:
                for (i = 0; i < height && i * (height / 3) < fileNameSize; i++)
                    for (j = 0; j < (width / 3) * 3 && i * (height / 3) + (j / 3) < fileNameSize; j++)
                    {
                        byte2bool((byte)fileName[i * (height / 3) + j / 3], ref t);
                        pixel = inputBitmap.GetPixel(j, i);
                        r = pixel.R;
                        g = pixel.G;
                        b = pixel.B;
                        byte2bool(r, ref rb);
                        byte2bool(g, ref gb);
                        byte2bool(b, ref bb);
                        if (j % 3 == 0)
                        {
                            rb[7] = t[0];
                            gb[7] = t[1];
                            bb[7] = t[2];
                        }
                        else if (j % 3 == 1)
                        {
                            rb[7] = t[3];
                            gb[7] = t[4];
                            bb[7] = t[5];
                        }
                        else
                        {
                            rb[7] = t[6];
                            gb[7] = t[7];
                        }
                        Color result = Color.FromArgb((int)bool2byte(rb), (int)bool2byte(gb), (int)bool2byte(bb));
                        outputBitmap.SetPixel(j, i, result);
                    }
                i--;
            }
            //write file (after file name):
            int tempj = j;

            for (; i < height && i * (height / 3) < endPosition - startPosition + FNSize && startPosition + i * (height / 3) < fileSize + FNSize; i++)
                for (j = 0; j < (width / 3) * 3 && i * (height / 3) + (j / 3) < endPosition - startPosition + FNSize && startPosition + i * (height / 3) + (j / 3) < fileSize + FNSize; j++)
                {
                    if (tempj != 0)
                    {
                        j = tempj;
                        tempj = 0;
                    }
                    byte2bool(fileContainer[startPosition + i * (height / 3) + j / 3 - FNSize], ref t);
                    pixel = inputBitmap.GetPixel(j, i);
                    r = pixel.R;
                    g = pixel.G;
                    b = pixel.B;
                    byte2bool(r, ref rb);
                    byte2bool(g, ref gb);
                    byte2bool(b, ref bb);
                    if (j % 3 == 0)
                    {
                        rb[layer] = t[0];
                        gb[layer] = t[1];
                        bb[layer] = t[2];
                    }
                    else if (j % 3 == 1)
                    {
                        rb[layer] = t[3];
                        gb[layer] = t[4];
                        bb[layer] = t[5];
                    }
                    else
                    {
                        rb[layer] = t[6];
                        gb[layer] = t[7];
                    }
                    Color result = Color.FromArgb((int)bool2byte(rb), (int)bool2byte(gb), (int)bool2byte(bb));
                    outputBitmap.SetPixel(j, i, result);

                }
            long tempFS = fileSize, tempFNS = fileNameSize;
            r = (byte)(tempFS % 100);
            tempFS /= 100;
            g = (byte)(tempFS % 100);
            tempFS /= 100;
            b = (byte)(tempFS % 100);
            Color flenColor = Color.FromArgb(r, g, b);
            outputBitmap.SetPixel(width - 1, height - 1, flenColor);

            r = (byte)(tempFNS % 100);
            tempFNS /= 100;
            g = (byte)(tempFNS % 100);
            tempFNS /= 100;
            b = (byte)(tempFNS % 100);
            Color fnlenColor = Color.FromArgb(r, g, b);
            outputBitmap.SetPixel(width - 2, height - 1, fnlenColor);

            return outputBitmap;
        }

        private static void byte2bool(byte inp, ref bool[] outp)
        {
            if (inp >= 0 && inp <= 255)
                for (short i = 7; i >= 0; i--)
                {
                    if (inp % 2 == 1)
                        outp[i] = true;
                    else
                        outp[i] = false;
                    inp /= 2;
                }
            else
                throw new Exception("Input number is illegal.");
        }

        private static byte bool2byte(bool[] inp)
        {
            byte outp = 0;
            for (short i = 7; i >= 0; i--)
            {
                if (inp[i])
                    outp += (byte)Math.Pow(2.0, (double)(7 - i));
            }
            return outp;
        }

        public static string JustFName(string path)
        {
            string output;
            int i;
            if (path.Length == 3)   // i.e: "C:\\"
                return path.Substring(0, 1);
            for (i = path.Length - 1; i > 0; i--)
                if (path[i] == '\\')
                    break;
            output = path.Substring(i + 1);
            return output;
        }
    }

}
