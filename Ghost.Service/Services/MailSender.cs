﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Mail;
using Ghost.Service.Interfaces;

namespace Ghost.Service.Services
{
    public class MailSender : IMailSender
    {
        public void SendMail(string To, string Subject, string Body)
        {
            MailMessage mailMessage = new MailMessage();
            mailMessage.To.Add(To);
            mailMessage.Subject = Subject;
            mailMessage.Body = Body;
            SmtpClient smtpClient = new SmtpClient("smtp.yandex.ru", 25);
            smtpClient.Send(mailMessage);
        }
    }
}
