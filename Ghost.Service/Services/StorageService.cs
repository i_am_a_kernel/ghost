﻿using System;
using System.Collections.Generic;
using Ghost.Data.Infrastructure;
using Ghost.Data.Repositories;
using Ghost.Domain;
using Ghost.Service.Interfaces;


namespace Ghost.Service.Services
{
    public class StorageService : IStorageService
    {
        private readonly IUserRepository _userRepository;
        private readonly IStorageRepository _storageRepository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IPartRepository _partRepository;
        private readonly IFileRepository _fileRepository;

        public StorageService(IUserRepository userRepository, 
            IStorageRepository storageRepository, 
            IUnitOfWork unitOfWork,
            IPartRepository partRepository,
            IFileRepository fileRepository)
        {
            this._userRepository = userRepository;
            this._storageRepository = storageRepository;
            this._fileRepository = fileRepository;
            this._partRepository = partRepository;
            this._unitOfWork = unitOfWork;
        }

        public IEnumerable<Storage> GetAllStorages()
        {
            var storages = _storageRepository.GetAll();
            return storages;
        }

        public IEnumerable<Storage> GetStoragesOfUser(int userId)
        {
            var user = _userRepository.GetById(userId);
            var storages = _storageRepository.GetMany(x => x.User == user);
            return storages;
        }

        public Storage GetStorageById(int id)
        {
            var storage = _storageRepository.GetById(id);
            return storage;
        }

        public void AppendStore(Storage storage)
        {
            _storageRepository.Add(storage);
            _unitOfWork.Commit();
        }

        public void DeleteStorage(int id)
        {
            var storage = _storageRepository.GetById(id);
            var files = _fileRepository.GetMany(x => x.Storage.StorageId == id);
            var parts = _partRepository.GetMany(x => x.Files.Storage.StorageId == id);
            foreach (var item in parts)
                _partRepository.Delete(item);
            foreach (var item in files)
                _fileRepository.Delete(item);
            _storageRepository.Delete(storage);
            _unitOfWork.Commit();
        }

        public void DeleteFile(int id)
        {
            var file = _fileRepository.GetById(id);
            var parts = _partRepository.GetMany(x => x.Files.FilesID == id);
            var storage = _storageRepository.Get(x => x.StorageId == file.Storage.StorageId);

            storage.CurrentSize -= file.FileSize;
            _storageRepository.Update(storage);

            foreach (var item in parts)
                _partRepository.Delete(item);
            _fileRepository.Delete(file);

            _unitOfWork.Commit();
        }

        public void SaveStorage()
        {
            _unitOfWork.Commit();
        }
    }
}
