﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Drawing;
using Ghost.Service.Interfaces;
using System.Xml;
using System.Text;
using System.IO;
using System.Collections;

using FlickrNet;

namespace Ghost.Service.Services
{
    public class ImageService : IImageService 
    {
        public Image ScaleImage(Image src, int size)
        {
            Image thumbnail;
            if (!(src.Height <= size || src.Width <= size))
            {
                System.Drawing.Size thumbnailSize = GetThumbnailSize(src, size);

                thumbnail = src.GetThumbnailImage(thumbnailSize.Width, thumbnailSize.Height, null, IntPtr.Zero);
            }
            else
            {
                thumbnail = src;
            }
            return thumbnail;
        }

        private System.Drawing.Size GetThumbnailSize(Image src, int size)
        {
            int maxPixels = size;

            int originalWidth = src.Width;
            int originalHeight = src.Height;

            double factor;
            if (originalWidth > originalHeight)
                factor = (double)maxPixels / originalWidth;
            else
                factor = (double)maxPixels / originalHeight;

            return new System.Drawing.Size((int)(originalWidth * factor), (int)(originalHeight * factor));
        }

        public ArrayList GetRandomImages()
        {
            var resultList = new ArrayList();

            string apikey = "141c17a38f68dbe8b2165c4436fe79aa";
            Flickr F = new Flickr(apikey);

            PhotoSearchOptions searchOptions = new PhotoSearchOptions();
            searchOptions.PerPage = 10;
            searchOptions.Extras = PhotoSearchExtras.LargeUrl;
            searchOptions.Tags = "nature";

            PhotoCollection set = F.PhotosSearch(searchOptions);

            foreach (var item in set)
            {
                resultList.Add(item.LargeUrl);
            }

            return resultList;
        }
    }
}
