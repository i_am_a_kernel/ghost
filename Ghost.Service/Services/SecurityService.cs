﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Web.Security;
using Ghost.Data.Infrastructure;
using Ghost.Data.Repositories;
using Ghost.Domain;
using Ghost.Service.Interfaces;

namespace Ghost.Service.Services
{
    public class SecurityService : ISecutiryService
    {
        private readonly IUserRepository _userRepository;
        private readonly IRoleRepository _roleRepository;
        private readonly IUnitOfWork _unitOfWork;

        public SecurityService(IUserRepository userRepository, IRoleRepository roleRepository, IUnitOfWork unitOfWork)
        {
            this._userRepository = userRepository;
            this._roleRepository = roleRepository;
            this._unitOfWork = unitOfWork;
        }

        public IEnumerable<User> GetUsers()
        {
            var users = _userRepository.GetAll();
            return users;
        }

        public User GetUserById(int id)
        {
            var user = _userRepository.GetById(id);
            return user;
        }

        public User GetUserById(string email)
        {
            var user = _userRepository.Get(x => x.Email == email);
            return user;
        }

        public void CreateUser(User user)
        {
            _userRepository.Add(user);
            _unitOfWork.Commit();
            _userRepository.AssignRole(user.UserId, 3);
            _unitOfWork.Commit();
        }

        public void DeleteUser(int id)
        {
            var user = _userRepository.GetById(id);
            _userRepository.Delete(user);
            _unitOfWork.Commit();
        }

        public void UpdateUser(User user)
        {
            _userRepository.Update(user);
            _unitOfWork.Commit();
        }

        public void AssignRole(int userId, int roleId)
        {
            _userRepository.AssignRole(userId, roleId);
            _unitOfWork.Commit();
        }

        public Role UserInRole(int userId)
        {
            var role = _userRepository.UserInRole(userId);
            return role;
        }

        public bool UserIsInRole(int userId, int roleId)
        {
            bool result = _userRepository.UserIsInRole(userId, roleId);
            return result;
        }

        public string CreateSalt()
        {
            RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
            byte[] buff = new byte[32];
            rng.GetBytes(buff);

            return Convert.ToBase64String(buff);
        }

        public bool IsUserExist(string email)
        {
            var user = _userRepository.Get(x => x.Email == email);
            if (user != null)
                return true;
            else
                return false;
        }

        public bool ValidateUser(string email, string hashedPassword)
        {
            var user = _userRepository.Get(x => x.Email == email);
            if (user == null)
                return false;
            if ((user.Password == hashedPassword) && (user.IsApproved))
                return true;
            else
                return false;
                
        }


        public void SaveUser()
        {
            _unitOfWork.Commit();
        }
    }
}
