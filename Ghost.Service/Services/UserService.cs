﻿using System;
using System.Collections.Generic;
using Ghost.Data.Infrastructure;
using Ghost.Data.Repositories;
using Ghost.Domain;
using Ghost.Service.Interfaces;

namespace Ghost.Service.Services
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;
        private readonly IGenderRepository _genderRepository;
        private readonly IUnitOfWork _unitOfWork;

        public UserService(IUserRepository userRepository, IGenderRepository genderRepository, IUnitOfWork unitOfWork)
        {
            this._genderRepository = genderRepository;
            this._userRepository = userRepository;
            this._unitOfWork = unitOfWork;
        }

        public void AssignGender(int userId, int genderId)
        {
            _userRepository.AssignGender(userId, genderId);
            _unitOfWork.Commit();
        }
    }
}
